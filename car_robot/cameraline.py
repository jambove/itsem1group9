#If SSH-ing, write "export DISPLAY=:0.0" in the terminal first

#Importing libraries
import RPi.GPIO as GPIO
from time import sleep
import time
import numpy as np
import cv2

#The following line makes it look at pin numbers, not GPIO numbers
GPIO.setmode(GPIO.BOARD)

#Assigning names to the pin numbers, m = middle , r = right, l = left
sensorm = 19
sensorr = 38
sensorl = 32

#FMotor means Front motor, 3 sensor side
FMotor1A = 37
FMotor1B = 36
FMotor1Enable = 31

#BMotor means Back motor, one/no sensor side
BMotor1A = 16
BMotor1B = 29
BMotor1Enable = 18

FMotor2A = 33
FMotor2B = 35 
FMotor2Enable = 40

BMotor2A = 11
BMotor2B = 7
BMotor2Enable = 13

#Setting up GPIOs to use
GPIO.setup(FMotor1A,GPIO.OUT)
GPIO.setup(FMotor1B,GPIO.OUT)
GPIO.setup(FMotor1Enable,GPIO.OUT)

GPIO.setup(BMotor1A,GPIO.OUT)
GPIO.setup(BMotor1B,GPIO.OUT)
GPIO.setup(BMotor1Enable,GPIO.OUT)

GPIO.setup(FMotor2A,GPIO.OUT)
GPIO.setup(FMotor2B,GPIO.OUT)
GPIO.setup(FMotor2Enable,GPIO.OUT)

GPIO.setup(BMotor2A,GPIO.OUT)
GPIO.setup(BMotor2B,GPIO.OUT)
GPIO.setup(BMotor2Enable,GPIO.OUT)

GPIO.setup(sensorm,GPIO.IN)
GPIO.setup(sensorr,GPIO.IN)
GPIO.setup(sensorl,GPIO.IN)
#GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#pin 29 was button, not actually used not it is pin 15, which DOESNT WORK

#Defining the actions of the wheel to make it easier in the end
#Fright means front right wheel spinning forward, Frightb means backward
def Fright():
	GPIO.output(FMotor2A,GPIO.LOW)
	GPIO.output(FMotor2B,GPIO.HIGH)
	GPIO.output(FMotor2Enable,GPIO.HIGH)

def Frightb():
	GPIO.output(FMotor2A,GPIO.HIGH)
	GPIO.output(FMotor2B,GPIO.LOW)
	GPIO.output(FMotor2Enable,GPIO.HIGH)


def Fleft():
	GPIO.output(FMotor1A,GPIO.HIGH)
	GPIO.output(FMotor1B,GPIO.LOW)
	GPIO.output(FMotor1Enable,GPIO.HIGH)

def Fleftb():
	GPIO.output(FMotor1A,GPIO.LOW)
	GPIO.output(FMotor1B,GPIO.HIGH)
	GPIO.output(FMotor1Enable,GPIO.HIGH)

#Fmotors spins two front wheels forward
def Fmotors():
	Fright()
	Fleft()

def Bright():
	GPIO.output(BMotor1A,GPIO.LOW)
	GPIO.output(BMotor1B,GPIO.HIGH)
	GPIO.output(BMotor1Enable,GPIO.HIGH)

def Brightb():
	GPIO.output(BMotor1A,GPIO.HIGH)
	GPIO.output(BMotor1B,GPIO.LOW)
	GPIO.output(BMotor1Enable,GPIO.HIGH)

def Bleft():
	GPIO.output(BMotor2A,GPIO.LOW)
	GPIO.output(BMotor2B,GPIO.HIGH)
	GPIO.output(BMotor2Enable,GPIO.HIGH)

def Bleftb():
	GPIO.output(BMotor2A,GPIO.HIGH)
	GPIO.output(BMotor2B,GPIO.LOW)
	GPIO.output(BMotor2Enable,GPIO.HIGH)

#Bmotors spins two back wheels forwards
def Bmotors():
	Bright()
	Bleft()

video_capture = cv2.VideoCapture(-1)

video_capture.set(3, 160)

video_capture.set(4, 120)

 
try:
	while(True):
		# Capture the frames
		ret, frame = video_capture.read()
		# Crop the image
		crop_img = frame[60:120, 0:160]
		# Convert to grayscale
		gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
		# Gaussian blur
		blur = cv2.GaussianBlur(gray,(5,5),0)
		# Color thresholding
		ret,thresh = cv2.threshold(blur,60,255,cv2.THRESH_BINARY_INV)
		# Find the contours of the frame
		_,contours,hierarchy = cv2.findContours(thresh.copy(), 1, cv2.CHAIN_APPROX_NONE)
		# Find the biggest contour (if detected)
		if len(contours) > 0:
			c = max(contours, key=cv2.contourArea)
			M = cv2.moments(c)
			if M["m00"] != 0:
				cx = int(M['m10']/M['m00'])
				cy = int(M['m01']/M['m00'])
			else:
				cx,cy = 0, 0	
			cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
			cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)
			cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)
			if cx >= 120:
				Fleft()
				Bleft()
				Frightb()
				Brightb()
			if cx < 120 and cx > 50:
				Fmotors()
				Bmotors()
			if cx <= 50:
				Fright()
				Bright()
				Fleftb()
				Bleftb()
		else:
			Fright()
			Bright()
			Fleftb()
			Bleftb()
		#Display the resulting frame
		cv2.imshow('frame',crop_img)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
except KeyboardInterrupt:
	GPIO.cleanup()
