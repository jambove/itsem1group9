#Importing libraries
import RPi.GPIO as GPIO
from time import sleep
import sys, termios, tty, os, time

#The following line makes it look at pin numbers, not GPIO numbers
GPIO.setmode(GPIO.BOARD)

#Assigning names to the pin numbers, m = middle , r = right, l = left
sensorm = 19
sensorr = 38
sensorl = 32

#FMotor means Front motor, 3 sensor side
FMotor1A = 37
FMotor1B = 36
FMotor1Enable = 31

#BMotor means Back motor, one/no sensor side
BMotor1A = 16
BMotor1B = 29 #changed from 15, it doesn't work. Changed the butotnto 15
BMotor1Enable = 18

FMotor2A = 33
FMotor2B = 35 
FMotor2Enable = 40

BMotor2A = 11
BMotor2B = 7
BMotor2Enable = 13

#Setting up GPIOs to use
GPIO.setup(FMotor1A,GPIO.OUT)
GPIO.setup(FMotor1B,GPIO.OUT)
GPIO.setup(FMotor1Enable,GPIO.OUT)

GPIO.setup(BMotor1A,GPIO.OUT)
GPIO.setup(BMotor1B,GPIO.OUT)
GPIO.setup(BMotor1Enable,GPIO.OUT)

GPIO.setup(FMotor2A,GPIO.OUT)
GPIO.setup(FMotor2B,GPIO.OUT)
GPIO.setup(FMotor2Enable,GPIO.OUT)

GPIO.setup(BMotor2A,GPIO.OUT)
GPIO.setup(BMotor2B,GPIO.OUT)
GPIO.setup(BMotor2Enable,GPIO.OUT)

GPIO.setup(sensorm,GPIO.IN)
GPIO.setup(sensorr,GPIO.IN)
GPIO.setup(sensorl,GPIO.IN)
#GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#pin 15 is button, not actually used, and neither does it work

#Defining the actions of the wheel to make it easier in the end
#Fright means front right wheel spinning forward, Frightb means backward
def Fright():
	GPIO.output(FMotor2A,GPIO.LOW)
	GPIO.output(FMotor2B,GPIO.HIGH)
	GPIO.output(FMotor2Enable,GPIO.HIGH)

def Frightb():
	GPIO.output(FMotor2A,GPIO.HIGH)
	GPIO.output(FMotor2B,GPIO.LOW)
	GPIO.output(FMotor2Enable,GPIO.HIGH)
	

def Fleft():
	GPIO.output(FMotor1A,GPIO.HIGH)
	GPIO.output(FMotor1B,GPIO.LOW)
	GPIO.output(FMotor1Enable,GPIO.HIGH)

def Fleftb():
	GPIO.output(FMotor1A,GPIO.LOW)
	GPIO.output(FMotor1B,GPIO.HIGH)
	GPIO.output(FMotor1Enable,GPIO.HIGH)

#Fmotors spins two front wheels forward
def Fmotors():
	Fright()
	Fleft()
	Bright()
	Bleft()

def Bright():
	GPIO.output(BMotor1A,GPIO.LOW)
	GPIO.output(BMotor1B,GPIO.HIGH)
	GPIO.output(BMotor1Enable,GPIO.HIGH)

def Brightb():
	GPIO.output(BMotor1A,GPIO.HIGH)
	GPIO.output(BMotor1B,GPIO.LOW)
	GPIO.output(BMotor1Enable,GPIO.HIGH)

def Bleft():
	GPIO.output(BMotor2A,GPIO.LOW)
	GPIO.output(BMotor2B,GPIO.HIGH)
	GPIO.output(BMotor2Enable,GPIO.HIGH)

def Bleftb():
	GPIO.output(BMotor2A,GPIO.HIGH)
	GPIO.output(BMotor2B,GPIO.LOW)
	GPIO.output(BMotor2Enable,GPIO.HIGH)

#Bmotors spins two back wheels forwards
def Bmotors():
	Frightb()
	Fleftb()
	Brightb()
	Bleftb()

def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

#main part of the code
#if middle sensor senses False, that is 0=black, all wheels spin forward
#if left sensor senses, that means car is to the right of the line,
#spins right side forward, left side backwards
#if right sensor senses, that means car is to the left of the line,
#spins right side backwards, left side forward
#if all sensors see white, it either stops, commented out, or spins left
try:
	while True:
		key_in = getch()
		if key_in == "p":
			GPIO.cleanup()
			exit(0)
			print("STOP!")

		elif key_in == "a":
			Fright()
			Bright()
			Fleftb()
			Bleftb()
		elif key_in == "d":
			Fleft()
			Bleft()
			Frightb()
			Brightb()
		elif key_in == "w":
			Fmotors()
		elif key_in == "s":
			Bmotors()

except:
	GPIO.cleanup()

#Cleans up the GPIO so it can be used by other python script
