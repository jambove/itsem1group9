# Python Program - Convert Miles to Kilometers
print("Enter 'x' for exit.")
miles = input("Enter value to convert miles into kilometers: ")
if miles == 'x':
    exit();
else:
    miles_x = float(miles)
    conversion_factor = 1.609344
    kilometers = miles_x * conversion_factor
    print("%0.2f kilometers\n" %(kilometers))

print("----------------------------------------------------------------")

# Python Program - Convert Kilometres to Miles
print("\nEnter 'x' for exit.")
km = input("Enter value to convert kilometres into miles: ")
if km == 'x':
    exit();
else:
    kilometres = float(km)
    conversion_factor = 0.621371
    miles = kilometres * conversion_factor
    print("%0.2f miles\n" %(miles))

print("----------------------------------------------------------------")

# Python Program - Golden Ratio
n = int(input("How many terms shall we include: "))

print("Choose an option below:")
print("1. Terms in fibonacci sequence")
fibonacci_list = [0,1]
for i in range(n - 1):
    fibonacci_list.append(fibonacci_list[i] + fibonacci_list[i+1])

print("2. Golden Ratios")
golden_ratio = [fibonacci_list[i]/(fibonacci_list[i-1])
                for i in range(2,len(fibonacci_list))]

choice = int(input("Enter choice (1/2): "))
if (choice == 1):
    print(fibonacci_list)
if (choice == 2):
    print(golden_ratio)
