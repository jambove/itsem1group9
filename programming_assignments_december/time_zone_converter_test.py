#!/usr/bin/python
import time

EST = time.time() + 3600 * -6
CST = time.time() + 3600 * -7
PST = time.time() + 3600 * -9
JST = time.time() + 3600 * 8
IST = time.time() + 3600 * 4.5
CT = time.time() + 3600 * -7
AWST = time.time() + 3600 * 7
ACWST = time.time() + 3600 * 7.75
ACST = time.time() + 3600 * 8.50
AEST = time.time() + 3600 * 9
ACDT = time.time() + 3600 * 9.50
AEDT = time.time() + 3600 * 10


# Local Time - Central European (CET)
print("\nLocal Time:", time.ctime())
# Central European (0 hours)
print("\nCET :", time.ctime())
# USA East Coast (-6 hours)
print("EST :", time.ctime(EST))
# USA Central (-7 hours)
print("CST :", time.ctime(CST))
# USA West Coast (-9 hours)
print("PST :", time.ctime(PST))
# Japan Standard Time (8 hours)
print("JST :", time.ctime(JST))
# Current Local Time in India (4.5 hours)
print("IST :", time.ctime(IST))
# Current Local Time in China (-7 hours)
print("CT :", time.ctime(CT))
# Australian Western Standard Time
print("AWST :", time.ctime(AWST))
# Australian Central Western Standard Time
print("ACWST :", time.ctime(ACWST))
# Australian Central Standard Time
print("ACST :", time.ctime(ACST))
# Australian Eastern Standard Time
print("AEST :", time.ctime(AEST))
# Australian Central Daylight Time
print("ACDT :", time.ctime(ACDT))
# Australian Eastern Daylight Time
print("AEDT :", time.ctime(AEDT))
