# Python Program - Convert Miles to Kilometers

print("Enter 'x' for exit.")
miles = input("Enter value to convert miles into kilometers: ")
if miles == 'x':
    exit();
else:
    miles_x = float(miles)
    conversion_factor = 1.609344
    kilometers = miles_x * conversion_factor
    print("%0.2f kilometers\n" %(kilometers))

# Python Program - Convert Kilometres to Miles

print("Enter 'x' for exit.")
km = input("Enter value to convert kilometres into miles: ")
if km == 'x':
    exit();
else:
    kilometres = float(km)
    conversion_factor = 0.621371
    miles = kilometres * conversion_factor
    print("%0.2f miles\n" %(miles))

# Python Program - Golden Ratio

print ("Let's compute the first few terms in the Fibonacci sequence.")

n = 1000 # How many terms shall we include?

# Iterative method, with values saved in a list
fiblist = [0,1]
for i in range(n - 1):
    fiblist.append(fiblist[i] + fiblist[i+1])
print (fiblist)

gratio = [fiblist[i]/float(fiblist[i-1]) for i in range(2,len(fiblist))]
print (gratio)
