from gpiozero import MCP3008
import RPi.GPIO as GPIO
from time import sleep

# BOARD: 37 35 33 31 

#MODE BCM
a1, b1 ,c1, d1 = 26, 19 ,13 ,6
a2, b2, c2, d2 = 21, 20, 16, 12

GPIO.setwarnings(False)

GPIO.setup([a1, b1, c1, d1], GPIO.OUT)
GPIO.setup([a2, b2, c2, d2], GPIO.OUT)

outp = {'00':[1,1,1,1],
	0: [0,0,0,0],
	1: [1,0,0,0],
	2: [0,1,0,0],
	3: [1,1,0,0],
	4: [0,0,1,0],
	5: [1,0,1,0],
	6: [0,1,1,0],
	7: [1,1,1,0],
	8: [0,0,0,1],
	9: [1,0,0,1]}
	
vref = 5
ch = 0
ch0 = 0

while True:
	with MCP3008(channel=ch) as reading:
		ch0 = reading.value * vref
		
		decimals = int(str(ch0)[2:4])		
		x = 16
		xt = 87
		xr = (xt-x)/99

		if decimals <= x:
			digit1 = digit2 = 0
			
		elif decimals < xt:
			for i in range(1,100):		
				if((str(x)[:2] == str(ch0)[2:4])):
					if i > 9:
						digit1, digit2 = i//10, i%10
					elif i < 10:
						digit1, digit2 = 0, i
					else:
						digit1 = digit2 = 0
					break
				x += xr
		else:
			digit1 = digit2 = 9

		print(ch0,digit1,digit2)
		GPIO.output([a1, b1, c1, d1], outp[digit1])
		sleep(0.1)
		GPIO.output([a2, b2, c2, d2], outp[digit2])
		sleep(5)
