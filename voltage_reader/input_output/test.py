import RPi.GPIO as GPIO
import time
import math
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
#SPAGHETTI

GPIO.setmode(GPIO.BCM)

A = 21
B = 20
C = 16
D = 12

GPIO.setup(A,GPIO.OUT)
GPIO.setup(B,GPIO.OUT)
GPIO.setup(C,GPIO.OUT)
GPIO.setup(D,GPIO.OUT)

CLK  = 11
MISO = 9
MOSI = 10
CS   = 8
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

def disp0():
	GPIO.output(A,GPIO.LOW)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.LOW)
def disp1():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.LOW)
def disp2():
	GPIO.output(A,GPIO.LOW)
	GPIO.output(B,GPIO.HIGH)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.LOW)
def disp3():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.HIGH)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.LOW)
def disp4():
	GPIO.output(A,GPIO.LOW)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.HIGH)
	GPIO.output(D,GPIO.LOW)
def disp5():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.HIGH)
	GPIO.output(D,GPIO.LOW)
def disp6():
	GPIO.output(A,GPIO.LOW)
	GPIO.output(B,GPIO.HIGH)
	GPIO.output(C,GPIO.HIGH)
	GPIO.output(D,GPIO.LOW)
def disp7():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.HIGH)
	GPIO.output(C,GPIO.HIGH)
	GPIO.output(D,GPIO.LOW)
def disp8():
	GPIO.output(A,GPIO.LOW)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.HIGH)
def disp9():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.LOW)
	GPIO.output(C,GPIO.LOW)
	GPIO.output(D,GPIO.HIGH)
def dispnot():
	GPIO.output(A,GPIO.HIGH)
	GPIO.output(B,GPIO.HIGH)
	GPIO.output(C,GPIO.HIGH)
	GPIO.output(D,GPIO.HIGH)	


print('Reading MCP3008 values')
print('| {0:>4} |'.format(*range(8)))
print('-' * 8)

try:
	while True:
		values = mcp.read_adc(0)
		print(values)
	    #divided = round(values/100)
	    #divided = -(-values//100)
	    #divided = math.floor(values//100)
	    #print(divided)
	    
		if values<100:
			disp0()
		elif 101<values<200:
			disp1()
		elif 201<values<300:
			disp2()
		elif 301<values<400:
			disp3()
		elif 401<values<500:
			disp4()
		elif 501<values<600:
			disp5()
		elif 601<values<700:
			disp6()
		elif 701<values<800:
			disp7()
		elif 801<values<900:
			disp8()
		elif 901<values:
			disp9()
		else:
			dispnot()
		time.sleep(0.50)
	   
finally:
    GPIO.cleanup()
    

