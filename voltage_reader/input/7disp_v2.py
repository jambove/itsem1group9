import RPi.GPIO as GPIO
from time import sleep
import time

# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

GPIO.setmode(GPIO.BCM)
#			A, B, C, D
Segments = (21,20,16,12)

CLK  = 11
MISO = 9
MOSI = 10
CS   = 8
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)


for Pins in Segments:
    GPIO.setup(Pins, GPIO.OUT)
    GPIO.output(Pins, 0)   
    
num = {' ':(1,1,1,1),
       '0':(0,0,0,0),
       '1':(1,0,0,0),
       '2':(0,1,0,0),
       '3':(1,1,0,0),
       '4':(0,0,1,0),
       '5':(1,0,1,0),
       '6':(0,1,1,0),
       '7':(1,1,1,0),
       '8':(0,0,0,1),
       '9':(1,0,0,1)}

n = 0;

try:
    while True:
        print("O something %d" %n)
        values = [0]*8       
        for loop in range(0, 4):            
            GPIO.output(Segments[loop], num[str(n)][loop])
            values[loop] = mcp.read_adc(loop)
        n += 1                              
        if n > 9:                           
            n = 0                           
        time.sleep(1)                     
finally:
    GPIO.cleanup()



#this is fucking nice I want to die
