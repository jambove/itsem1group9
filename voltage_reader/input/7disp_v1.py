import RPi.GPIO as GPIO
from time import sleep
import time

GPIO.setmode(GPIO.BOARD)

#Inputs
#CLK = 31
#DOUT = 33
#DIN = 35
#CS_SHDN = 37


#			A, B, C, D
Segments = (40,38,36,32)

for Pins in Segments:
    GPIO.setup(Pins, GPIO.OUT)
    GPIO.output(Pins, 0)   
    
num = {' ':(1,1,1,1),
       '0':(0,0,0,0),
       '1':(1,0,0,0),
       '2':(0,1,0,0),
       '3':(1,1,0,0),
       '4':(0,0,1,0),
       '5':(1,0,1,0),
       '6':(0,1,1,0),
       '7':(1,1,1,0),
       '8':(0,0,0,1),
       '9':(1,0,0,1)}

n = 0;

try:
    while True:
        print("O something %d" %n)       
        for loop in range(0, 4):            
            GPIO.output(Segments[loop], num[str(n)][loop])
        n += 1                              
        if n > 9:                           
            n = 0                           
        time.sleep(1)                     
finally:
    GPIO.cleanup()
