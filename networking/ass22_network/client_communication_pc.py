import random
import socket
import os
import time


def measure_temp():
    temp = os.popen("vcgencmd measure_temp").readline()
    return (temp.replace("temp=", ""))


def __main__():

    HOST = '100.64.12.235' # The server's hostname or IP address
    PORT = 65430 # The port to send data to on the server
    mySensorReadings = 'go' # The application layer protocol


    a = []
    for i in range(2):
        a.append(str(random.randint(10,99)))


    a.append("48.9'C")
    data = '-'.join(a)
    print(data)
    #print(data[-1])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))

    mySensorReadings = data
    print(mySensorReadings.encode(('utf-8')))
    s.sendall(mySensorReadings.encode('utf-8'))


__main__()
