import socket
from time import sleep
from random import randint
from datetime import datetime

host = '192.168.8.23'
port = 64530

def generate_temps():
    return [[randint(1,3)+x-randint(1,5) for x in range(y,y+10)] for y in
range(50,55)]

def list_del(l):
    del(l[0][0])
    if(l[0] == []):
        del(l[0])

def main():
    s = socket.socket()
    s.connect((host, port))

    temps = generate_temps()

    while True:
        s.sendall(str(temps[0][0]).encode())

        list_del(temps)
        if temps == []: temps = generate_temps()
        sleep(1)

main()