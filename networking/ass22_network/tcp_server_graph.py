import socket
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from datetime import datetime

host = '192.168.8.23'
port = 64530

def plot_setup():
    global x, y, c, fig, yx
    plt.style.use('ggplot')
    fig, yx = plt.subplots(figsize=(13,6))
    yx.set(xlabel='Time', ylabel='Temperature (C°)', title='Temperature Monitor')

    x = []
    y = []
    c = 0

def animate(q):
    global x, y, c

    if c > 50:
        x = []
        y = []
        c = 0

    data = connection.recv(2).decode()
    time = datetime.now().strftime("%H: %M: %S")

    y.append(int(data))
    x.append(str(time))

    xi = [i for i in range(0, len(x))]

    yx.clear()

    yx.plot(xi, y, '-o', color ='r', alpha=0.8)
    plt.xticks(xi, x, rotation=90)

    c+=1

def main():
    global connection

    plot_setup()

    s = socket.socket()
    s.bind((host, port))
    s.listen(5)

    connection, fromAddress = s.accept()

    if connection:
        ani = animation.FuncAnimation(fig, animate, interval=2)
        plt.show()

main()