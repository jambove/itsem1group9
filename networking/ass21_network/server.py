#https://www.geeksforgeeks.org/python-program-split-join-string/
import socket # Fetch the socket module

HOST = '10.140.77.152' # Standard loopback interface address (localhost)
PORT = 65430 # Port to listen on (non-privileged ports are > 1023)
DataCommingIn = True

#pickle.loads(recvd_data)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
print('Awaiting connection on IP: ',s.getsockname()[0],\
						  ' Port: ',s.getsockname()[1])
connection, fromAddress = s.accept() # Wait and create connection socket
print('Connection from:', fromAddress)

while DataCommingIn:
    receivedData = connection.recv(14)
    print(receivedData.decode('utf-8'))
    if not receivedData:
        DataCommingIn = False
connection.close()
print('Connection closed')