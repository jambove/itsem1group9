#!/usr/bin/env python3
###########################
# ASS 23 - CLIENT PROGRAM #
###########################
import socket					# Fetch the socket module
import pickle       			# Fetch the pickle module
import time						# Fetch the time module
from random import randint		# Fetch the randint module
from datetime import datetime	# Fetch the datetime module
import ssl 						# Fetch the ssl module

HOST = '127.0.0.1'      		# The server's hostname or IP address
PORT = 65432            		# The port to send data to on the server
DELAY = 1						# Set defualt delay to one second

def randTemp():

        Time = str(datetime.now())[11:19]
        Temps = randint(-10, 25)
        time.sleep(DELAY)
        return Temps, Time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Require a certificate from the server. We use a self-signed certificate
# so here ca_certs must be the server certificate itself.
mySSLSocket = ssl.wrap_socket(s,
                           ca_certs="serverCertificate.crt",
                           cert_reqs=ssl.CERT_REQUIRED)

mySSLSocket.connect((HOST, PORT))

while True:
	try:
		mySensorReadings = randTemp()
		mySSLSocket.sendall(pickle.dumps(mySensorReadings))
		print(str(datetime.now()), " >>> Data sent <<< ")
	except ValueError:	
		mySSLSocket.close()
		s.close()
		print(' >>> Connection closed')